import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {UserServiceService} from '../../user-service.service'
import { Location } from '@angular/common';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

  users : any;
  constructor(private userServiceService : UserServiceService, private route: ActivatedRoute, private location: Location) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.userServiceService.getUser(params.get('id')).subscribe(c =>{
       this.users = c;
       console.log(this.users);
       })   
       });
  } 
  back() {
    this.location.back(); 
  }

}
