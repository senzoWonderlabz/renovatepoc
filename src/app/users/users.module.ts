import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UserListComponent } from './user-list/user-list.component';
import { MatCardModule } from '@angular/material/card';
import { FormsModule } from '@angular/forms';
import { OrdersComponent } from './orders/orders.component';




@NgModule({
  declarations: [UserListComponent, OrdersComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    MatCardModule,
    FormsModule

  ]
})
export class UsersModule { }
