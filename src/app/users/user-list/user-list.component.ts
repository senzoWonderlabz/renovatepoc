import { Component, OnInit } from '@angular/core';
import {UserServiceService} from '../../user-service.service'

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
users : any;
firstName :any;

  constructor( private userServiceService : UserServiceService) {
    this 
   }

  ngOnInit(): void {
    this.userServiceService.getUsers().subscribe(
      data => {
        this.users = data;
        //JS Sort fn with callback Fn
        this.users.sort(function(a, b){
          var x = a.name.toLowerCase();
          var y = b.name.toLowerCase();
          if (x < y) {return -1;}
          if (x > y) {return 1;}
          return 0;
        });
      }
    )
   
  }
  Search(){
    if(this.firstName == ""){
      this.ngOnInit();
    }else{
      this.users = this.users.filter(res => {
        return res.name.toLowerCase().match(this.firstName.toLowerCase());
      })
    }
  }

  
}
