import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrdersComponent } from './orders/orders.component';
import {UserListComponent} from './user-list/user-list.component';

const routes: Routes = [
  {path:'', component: UserListComponent},
  {path:':id/orders', component:OrdersComponent },
];
 
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
