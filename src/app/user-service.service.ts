import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {
  constructor(private http:HttpClient) { }
  getUsers(){
    let url ="https://5fb6135636e2fa00166a4d8a.mockapi.io/api/users"
    return this.http.get(url);
  }

  getUser(userId){
    let url ="https://5fb6135636e2fa00166a4d8a.mockapi.io/api/users";
    return this.http.get(`${url}/${userId}/orders`) 
   }

}
