import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FormsModule } from '@angular/forms'

const routes: Routes =
  [
    { path: '', loadChildren: () => import('./pages/welcome/welcome.module').then(m => m.WelcomeModule) },
    {path: 'users', loadChildren:() =>import('./users/users.module').then(mod=>mod.UsersModule)},
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes), FormsModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
